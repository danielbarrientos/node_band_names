
const express = require('express');
const path = require('path');
require('dotenv').config();

//App express
const app = express();

// Nodejs server
const server = require('http').createServer(app);
module.exports.io = require('socket.io')(server);
require('./sockets/socket');

//public path
const publicPath = path.resolve( __dirname, 'public');

app.use( express.static(publicPath))

server.listen(process.env.PORT, (error)=>  {

    if(error) throw new Error(error);

    console.log('Server on port !!: ', process.env.PORT);
});
